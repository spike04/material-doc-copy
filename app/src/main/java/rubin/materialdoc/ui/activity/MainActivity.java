package rubin.materialdoc.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rubin.materialdoc.R;
import rubin.materialdoc.ui.adapter.TutorialsAdapter;
import rubin.materialdoc.ui.model.ItemsArr;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.recyclerView)
    RecyclerView recyclerView;

    List<ItemsArr> itemsArrList;
    TutorialsAdapter adapter;
    private String jsonData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        readDataFromAsset();
        parseJSONData(jsonData);

        setUpRecyclerView();

    }

    private void setUpRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        adapter = new TutorialsAdapter(this, itemsArrList);
        recyclerView.setAdapter(adapter);
    }

    private void parseJSONData(String jsonData) {
        try {
            JSONArray jsonArray = new JSONArray(jsonData);
            itemsArrList = new ArrayList<>();
            Log.e("JSONArray", jsonArray.length() + "");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONArray itemArrValue = jsonObject.getJSONArray("itemsArr");

                Log.e("ItemArrs", itemArrValue.length() + "");
                for (int j = 0; j < itemArrValue.length(); j++) {

                    JSONObject jsonItemArr = itemArrValue.getJSONObject(j);

                    ItemsArr itemsArr = new ItemsArr();
                    itemsArr.setId(jsonItemArr.getInt("id"));
                    itemsArr.setTitle(jsonItemArr.getString("title"));
                    itemsArr.setDescription(jsonItemArr.getString("description"));
                    itemsArr.setImage(jsonItemArr.getString("image"));

                    itemsArrList.add(itemsArr);
                }
            }
            Log.e("JSONData", itemsArrList.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void readDataFromAsset() {
        StringBuffer buf = new StringBuffer();
        try {
            InputStream json = getAssets().open("data.json");
            BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;

            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
            in.close();

            jsonData = buf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
