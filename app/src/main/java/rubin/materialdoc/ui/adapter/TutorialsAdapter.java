package rubin.materialdoc.ui.adapter;

import android.app.Activity;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rubin.materialdoc.R;
import rubin.materialdoc.ui.activity.CheckBoxActivity;
import rubin.materialdoc.ui.activity.CircularProgressActivity;
import rubin.materialdoc.ui.activity.FlatButtonActivity;
import rubin.materialdoc.ui.activity.InputActivity;
import rubin.materialdoc.ui.activity.InputFullWidthActivity;
import rubin.materialdoc.ui.activity.InputMultiLineActivity;
import rubin.materialdoc.ui.activity.InputSingleLineActivity;
import rubin.materialdoc.ui.activity.LinearProgressActivity;
import rubin.materialdoc.ui.activity.RadioButtonActivity;
import rubin.materialdoc.ui.activity.RaisedButtonActivity;
import rubin.materialdoc.ui.activity.RatingBarActivity;
import rubin.materialdoc.ui.activity.SwitchActivity;
import rubin.materialdoc.ui.activity.tab.TabActivity;
import rubin.materialdoc.ui.activity.tab.TabIconActivity;
import rubin.materialdoc.ui.activity.tab.TabMultiActivity;
import rubin.materialdoc.ui.activity.tab.TabStyledActivity;
import rubin.materialdoc.ui.model.ItemsArr;

/**
 * Created by rgh on 11/22/2015.
 */
public class TutorialsAdapter extends RecyclerView.Adapter<TutorialsAdapter.MyHolder> {

    Context context;
    List<ItemsArr> jsonDataList;

    public TutorialsAdapter(Context context, List<ItemsArr> jsonDataList) {
        this.context = context;
        this.jsonDataList = jsonDataList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, final int position) {

        holder.txtTitle.setText(jsonDataList.get(position).getTitle());
        holder.txtDescription.setText(jsonDataList.get(position).getDescription());
        holder.itemCard.setTag(position);
        holder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {
                    //Buttons
                    case 0:
                        RaisedButtonActivity.start((Activity) context);
                        break;
                    case 1:
                        FlatButtonActivity.start((Activity) context);
                        break;

                    //Progress
                    case 2:
                        CircularProgressActivity.start((Activity) context);
                        break;
                    case 3:
                        LinearProgressActivity.start((Activity) context);
                        break;

                    //Selection controls
                    case 4:
                        CheckBoxActivity.start((Activity) context);
                        break;
                    case 5:
                        RadioButtonActivity.start((Activity) context);
                        break;
                    case 6:
                        SwitchActivity.start((Activity) context);
                        break;

                    //edit fields
                    case 7:
                        InputActivity.start((Activity) context);
                        break;
                    case 8:
                        InputSingleLineActivity.start((Activity) context);
                        break;
                    case 9:
                        InputMultiLineActivity.start((Activity) context);
                        break;
                    case 10:
                        InputFullWidthActivity.start((Activity) context);
                        break;

                    //tab
                    case 11:
                        TabActivity.start((Activity) context);
                        break;
                    case 12:
                        TabIconActivity.start((Activity) context);
                        break;
                    case 13:
                        TabMultiActivity.start((Activity) context);
                        break;
                    case 14:
                        TabStyledActivity.start((Activity) context);
                        break;


                    //other
                    case 15:
                        RatingBarActivity.start((Activity) context);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return jsonDataList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.item_card)
        CardView itemCard;
        @InjectView(R.id.txtTitle)
        TextView txtTitle;
        @InjectView(R.id.txtDescription)
        TextView txtDescription;

        public MyHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
